Datasets of Control Logic Injection Attack
================

These datasets are for the researches of industrial control system (ICS) security. They consist of training datasets (normal), datasets of traditional control logic inejction attacks, and datasets of (new) stealthy control logic injection attacks (i.e., Data Execution, Fragmentation and Noise Padding), for Schneider Electric's Modicon M221 PLC and Allen-Bradley's MicroLogix 1400 PLC.


Datasets for Schneider Electric's Modicon M221 PLCs
-------------
* Training Dataset
* Traditioanl Attack Dataset
* Dataset of the Data Execution & Fragmentation and Noise Padding Attack


Datasets for Allen-Bradley's MicroLogix 1400 PLCs
------------
* Training Dataset
* Traditioanl Attack Dataset
* Dataset of the Fragmentation and Noise Padding Attack


Paper
------------
Hyunguk Yoo and Irfan Ahmed, Control Logic Injection Attacks on Industrial Control Systems, IFIP SEC 2019